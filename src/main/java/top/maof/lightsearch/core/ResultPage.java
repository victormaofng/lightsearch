package top.maof.lightsearch.core;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResultPage<T> {
    private List<T> list;
    private long total;
}
