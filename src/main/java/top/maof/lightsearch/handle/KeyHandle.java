package top.maof.lightsearch.handle;

import org.apache.lucene.document.*;
import org.apache.lucene.index.IndexableField;
import top.maof.lightsearch.annotation.Key;
import top.maof.lightsearch.map.Mapping;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * 主键处理器
 * 只处理@Key属性
 */
@Handle.Shared
public class KeyHandle implements Handle {
    @Override
    public void indexHandle(Entity entity, Index index, HandleChain handleChain) throws Exception {
        Iterator<Field> iterator = entity.getFields().iterator();

        while (iterator.hasNext()) {
            Field field = iterator.next();
            Key key = field.getAnnotation(Key.class);

            Document document = index.getDocument();
            if (key != null) {

                String name = handleChain.fieldName(entity.getType().value(), key.value());

                field.setAccessible(true);

                Object value = null;

                try {
                    value = field.get(entity.getT());
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }

                Mapping mapping = handleChain.getMapping(field.getType());
                if (mapping != null) {
                    List<IndexableField> indexableFields = mapping.javaMappingIndex(
                            new Mapping.Parms(value, name, false, true, key.isStored()));

                    indexableFields.forEach(indexableField -> {
                        document.add(indexableField);
                    });
                }


                // iterator.remove();
                break;
            }
        }

        handleChain.indexHandle(entity, index, handleChain);
    }

    @Override
    public void searchHandle(Entity entity, Index index, HandleChain handleChain) throws Exception {
        Iterator<Field> iterator = entity.getFields().iterator();

        while (iterator.hasNext()) {
            Field field = iterator.next();
            Key key = field.getAnnotation(Key.class);

            if (key != null) {
                // 不保存
                if (!key.isStored()) {
                    // iterator.remove();
                    break;
                }

                Document document = index.getDocument();

                field.setAccessible(true);

                Object t = entity.getT();

                String name = handleChain.fieldName(entity.getType().value(), key.value());

                Mapping mapping = handleChain.getMapping(field.getType());

                if (mapping != null) {

                    Object indexValue = mapping.indexMappingJava(Arrays.asList(document.getFields(name)));

                    try {
                        field.set(t, indexValue);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }

                // iterator.remove();
                break;
            }

        }

        handleChain.searchHandle(entity, index, handleChain);
    }
}
