package top.maof.lightsearch.tag;

public interface Tag {

    String preTag();

    String postTag();

}
