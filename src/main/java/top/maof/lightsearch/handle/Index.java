package top.maof.lightsearch.handle;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.search.Query;

/**
 * 索引模型类,保存由Entity转化而来的信息
 * 包括  Document、分词器、查询对象等等
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Index {

    private Document document;

    private Analyzer analyzer;

    private Query query;


    public Index(Document document) {
        this.document = document;
    }

    public Index(Document document, Analyzer analyzer) {
        this.document = document;
        this.analyzer = analyzer;
    }
}
