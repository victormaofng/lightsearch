package top.maof.lightsearch.map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.lucene.index.IndexableField;

import java.util.List;

/**
 * 映射器
 * <p>
 * 将实体类对象转化为IndexField对象
 *
 * <p>
 * javaType  <=======>  IndexFieldType
 */
public interface Mapping {

    List<IndexableField> javaMappingIndex(Parms parms) throws Exception;


    Object indexMappingJava(List<IndexableField> list) throws Exception;


    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    class Parms {

        private Object javaValue;

        private String fieldName;

        private boolean isTokenized;

        // 是否索引
        private boolean isIndexed;

        // 是否存储
        private boolean isStored;
    }

}
