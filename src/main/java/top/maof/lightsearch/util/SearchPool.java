package top.maof.lightsearch.util;

import java.util.concurrent.*;

public class SearchPool {
    /**
     * 当任务提交给ThreadPoolExecutor 线程池中，先检查核心线程数是否已经全部使用，
     * 如果没有交由核心线程去执行任务，如果核心线程数已经全部占用，则将任务添加到队列里面，
     * 如果队列已经占满，比较当前线程池的中线程的数量是不是与超过maximumPoolSize，
     * 如果没有查过则创建线程去执行，也就是说线程池最多可以接受多少任务呢？
     * 就是maximumPoolSize+队列的大小。当线程池中的线程的数量大于corePoolSize数量有空闲线程则执行回收，
     * 回收时间是keepAliveTime，单位是unit，都是初始化的时候设置的。
     * <p>
     * corePoolSize简单设置方法：
     * cpu密集型：cpu个数
     * io密集：cpu个数 * 2
     */
    private static int CORE_POOL_SIZE = Runtime.getRuntime().availableProcessors();

    private static ExecutorService EXECUTOR = new ThreadPoolExecutor(CORE_POOL_SIZE,
            CORE_POOL_SIZE * 2, 60, TimeUnit.SECONDS
            , new ArrayBlockingQueue<Runnable>(16), Executors.defaultThreadFactory(), new ThreadPoolExecutor.AbortPolicy());

    public static ExecutorService get() {
        return EXECUTOR;
    }

    public static void execute(Runnable runnable) {
        EXECUTOR.execute(runnable);
    }

    public static void close() {
        EXECUTOR.shutdown();
    }
}
