package top.maof.lightsearch.converter;

import java.util.Date;

public class DateToLongConverter implements Converter<Date, Long> {
    @Override
    public Class propertyType() {
        return Date.class;
    }

    @Override
    public Class filedType() {
        return Long.class;
    }

    @Override
    public Long writeCovert(Date value) {
        return value.getTime();
    }

    @Override
    public Date readCovert(Long fieldValue) {
        return new Date(fieldValue);
    }
}
