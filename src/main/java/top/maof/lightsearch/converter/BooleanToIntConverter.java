package top.maof.lightsearch.converter;

/**
 * 将boolean转换为int
 * 便于保存
 */
public class BooleanToIntConverter implements Converter<Boolean, Integer> {

    @Override
    public Class propertyType() {
        return Boolean.class;
    }

    @Override
    public Class filedType() {
        return Integer.class;
    }

    @Override
    public Integer writeCovert(Boolean value) {
        return value ? 1 : 0;
    }

    @Override
    public Boolean readCovert(Integer fieldValue) {
        return fieldValue == 1;
    }

}
