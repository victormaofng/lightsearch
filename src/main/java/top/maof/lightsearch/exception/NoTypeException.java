package top.maof.lightsearch.exception;

public class NoTypeException extends LightException {
    public NoTypeException() {
        super("实体类类没有设置Type");
    }
}
