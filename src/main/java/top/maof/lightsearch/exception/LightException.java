package top.maof.lightsearch.exception;

public class LightException extends Exception {
    public LightException(String msg) {
        super(msg);
    }
}
