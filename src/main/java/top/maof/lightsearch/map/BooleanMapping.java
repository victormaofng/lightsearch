package top.maof.lightsearch.map;

import org.apache.lucene.document.StoredField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.IndexableField;

import java.util.ArrayList;
import java.util.List;

public class BooleanMapping implements Mapping {

    @Override
    public List<IndexableField> javaMappingIndex(Parms parms)  throws Exception{
        List<IndexableField> list = new ArrayList<>();
        if (parms.isIndexed()) {
            //只索引但不分词 StringField(FieldName, FieldValue,Store.YES))
            IndexableField indexableField = parms.isStored() ? new StringField(parms.getFieldName(), parms.getJavaValue() + "",
                    org.apache.lucene.document.Field.Store.YES)
                    : new StringField(parms.getFieldName(), parms.getJavaValue() + "",
                    org.apache.lucene.document.Field.Store.NO);
            list.add(indexableField);
        } else if (parms.isStored()) {
            list.add(new StoredField(parms.getFieldName(), parms.getJavaValue() + ""));
        }
        return list;
    }

    @Override
    public Object indexMappingJava(List<IndexableField> list)  throws Exception{
        if (list.isEmpty()) {
            return null;
        }
        IndexableField indexableField = list.get(0);
        return Boolean.parseBoolean(indexableField.stringValue());
    }
}
