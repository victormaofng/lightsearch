package top.maof.lightsearch.exception;

public class NoKeyException extends LightException {
    public NoKeyException() {
        super("没有设置Key");
    }
}
