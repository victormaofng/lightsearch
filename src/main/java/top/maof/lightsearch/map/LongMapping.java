package top.maof.lightsearch.map;

import org.apache.lucene.document.LongPoint;
import org.apache.lucene.document.StoredField;
import org.apache.lucene.index.IndexableField;

import java.util.ArrayList;
import java.util.List;

public class LongMapping implements Mapping {
    @Override
    public List<IndexableField> javaMappingIndex(Parms parms) throws Exception {

        List<IndexableField> list = new ArrayList<>();

        if (parms.isIndexed()) {
            list.add(new LongPoint(parms.getFieldName(), (long) parms.getJavaValue()));
        }

        if (parms.isStored()) {
            list.add(new StoredField(parms.getFieldName(), (long) parms.getJavaValue()));
        }

        return list;
    }

    @Override
    public Object indexMappingJava(List<IndexableField> list) throws Exception {
        if (list.isEmpty()) {
            return null;
        }
        IndexableField indexableField = list.get(0);
        return (long) indexableField.numericValue();
    }
}
