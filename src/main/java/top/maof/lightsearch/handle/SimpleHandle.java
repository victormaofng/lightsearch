package top.maof.lightsearch.handle;

import lombok.extern.slf4j.Slf4j;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexableField;
import top.maof.lightsearch.annotation.Property;
import top.maof.lightsearch.converter.Converter;
import top.maof.lightsearch.map.Mapping;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * 简单处理器
 * 只处理Property中不是Converter.class的属性
 */
@Handle.Shared
@Slf4j
public class SimpleHandle implements Handle {


    @Override
    public void indexHandle(Entity entity, Index index, HandleChain handleChain) throws Exception {
        Iterator<Field> iterator = entity.getFields().iterator();

        while (iterator.hasNext()) {
            Field field = iterator.next();
            Property property = field.getAnnotation(Property.class);

            // 没有Converter
            if (property != null && property.convert() == Converter.class) {

                Document document = index.getDocument();
                String name = handleChain.fieldName(entity.getType().value(), property.value());
                field.setAccessible(true);
                try {
                    Object value = field.get(entity.getT());
                    if (value == null) {
                        continue;
                    }
                    Mapping mapping = handleChain.getMapping(field.getType());
                    if (mapping != null) {
                        List<IndexableField> indexableFields = mapping.javaMappingIndex(
                                new Mapping.Parms(value, name, property.isTokenized(), property.isIndexed(), property.isStored()));
                        indexableFields.forEach(indexableField -> {
                            document.add(indexableField);
                        });
                    }

                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                    log.error("创建索引失败，error:{}", e.getMessage());
                }

                // 移除该Field，以防重复操作
                // iterator.remove();
            }
        }
        handleChain.indexHandle(entity, index, handleChain);
    }

    @Override
    public void searchHandle(Entity entity, Index index, HandleChain handleChain) throws Exception {

        Iterator<Field> iterator = entity.getFields().iterator();
        while (iterator.hasNext()) {
            Field field = iterator.next();
            Property property = field.getAnnotation(Property.class);
            // 没有Converter
            if (property != null && property.convert() == Converter.class) {
                if (!property.isStored()) {
                    // iterator.remove();
                    continue;
                }
                Document document = index.getDocument();
                String name = handleChain.fieldName(entity.getType().value(), property.value());
                field.setAccessible(true);
                Mapping mapping = handleChain.getMapping(field.getType());
                if (mapping != null) {
                    Object indexValue = mapping.indexMappingJava(Arrays.asList(document.getFields(name)));
                    try {
                        field.set(entity.getT(), indexValue);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                        log.error("创建索引失败，error:{}", e.getMessage());
                    }
                }

                // 移除该Field，以防重复操作
                // iterator.remove();
            }
        }

        handleChain.searchHandle(entity, index, handleChain);
    }
}
