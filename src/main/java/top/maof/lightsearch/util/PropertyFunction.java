package top.maof.lightsearch.util;

import java.io.Serializable;

@FunctionalInterface
public interface PropertyFunction<T> extends Serializable {
    Object get(T t);
}
