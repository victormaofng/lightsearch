package top.maof.lightsearch.test;

import lombok.Data;
import lombok.ToString;
import top.maof.lightsearch.annotation.Key;
import top.maof.lightsearch.annotation.Property;
import top.maof.lightsearch.annotation.Type;
import top.maof.lightsearch.converter.DateToLongConverter;
import top.maof.lightsearch.converter.ListToStringConverter;
import top.maof.lightsearch.tag.SpanTag;

import java.util.Date;
import java.util.List;
import java.util.Set;

@Data
@ToString
@Type(value = "X")
public class X {
    private static int count = 40;
    @Key("id")
    private int id = count++;
    @Property(value = "name", tag = SpanTag.class, weight = 2)
    private String name = "mao";
    @Property(value = "age", isTokenized = false, weight = 3)
    private int age = 24;
    @Property(value = "time", isTokenized = false)
    private long time = System.currentTimeMillis();
    @Property(value = "gender", isTokenized = false)
    private boolean gender = false;
    @Property(value = "date", convert = DateToLongConverter.class)
    private Date date = new Date();
    @Property(value = "list", isTokenized = false, isIndexed = false, weight = 10,
            convert = ListToStringConverter.class)
    private List<Integer> list;
}
