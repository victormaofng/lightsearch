package top.maof.lightsearch.map;

import org.apache.lucene.document.DoublePoint;
import org.apache.lucene.document.StoredField;
import org.apache.lucene.index.IndexableField;

import java.util.ArrayList;
import java.util.List;

public class DoubleMapping implements Mapping {
    @Override
    public List<IndexableField> javaMappingIndex(Parms parms) throws Exception {

        List<IndexableField> list = new ArrayList<>();

        if (parms.isIndexed()) {
            list.add(new DoublePoint(parms.getFieldName(), (double) parms.getJavaValue()));
        }

        if (parms.isStored()) {
            list.add(new StoredField(parms.getFieldName(), (double) parms.getJavaValue()));
        }
        return list;
    }

    @Override
    public Object indexMappingJava(List<IndexableField> list)  throws Exception{
        if (list.isEmpty()) {
            return null;
        }
        IndexableField indexableField = list.get(0);
        return (double) indexableField.numericValue();

    }
}
