# light_search

#### 介绍
  通过简单的几个注解和几行代码，就可以自动完成实体类到document的相互转换


#### 软件架构
  通过简单的注解和反射代码，使document和实体类的转换变得简单


#### 安装教程

1.  下载到本地，使用mvn install到本地仓库
2.  在新项目中的pom文件中引入

#### 使用说明

1. 在实体类中注明`Type` `Key` `Property`

   ```java
   // 在实体类X上使用Type注解
   @Type(value = "X") 
   public class X {
       private static int count = 0;
   
       // 主键注解
       @Key("id")
       private int id = count++;
   
       // 其他属性注解
       @Property(value = "name", isStored = true, isTokenized = true, isIndexed = true)
       private String name = "mao";
   
       @Property(value = "age", isStored = true, isTokenized = false, isIndexed = true)
       private int age = 24;
   
   
       @Property(value = "time", isStored = true, isTokenized = false, isIndexed = true)
       private long time = System.currentTimeMillis();
   
   
       @Property(value = "gender", isStored = true, isTokenized = false, isIndexed = true)
       private boolean gender = false;
   
   
       @Property(value = "date", isStored = true, isTokenized = true,
               isIndexed = true, convert = DateToLongConverter.class)
       private Date date = new Date();
   }
   ```

2. 创建索引

   ```java
          // LightSearch核心类，可以创建索引和搜索         
          LightSearch lightSearch = LightSearch.getInstance("C:\\Users\\Administrator\\Desktop\\x");
  
          String[] names = new String[]{"全文检索引擎lucene", "培训机构", "全球直播",
                  "简单工厂模式", "工厂方法模式", "原型模式", "策略模式"};
  
          List<X> list = new ArrayList<>();
          int i = 0;
          while (i < names.length) {
              X x = new X();
              x.setName(names[i]);
              x.setAge(i * 2);
              list.add(x);
              i++;
          }
  
          try {
              lightSearch.write(list);
          } catch (Exception e) {
              e.printStackTrace();
          }
  
          lightSearch.close();
   ```

   

3. 搜索索引

   ```java
            LightSearch lightSearch = LightSearch.getInstance("C:\\Users\\Administrator\\Desktop\\x");
            // 搜索策略构建类
            QueryBuilder queryBuilder = new QueryBuilder();
            queryBuilder.tokenizeQuery("X.name", "Lucene", new IKAnalyzer(), BooleanClause.Occur.MUST);
            lightSearch.searchHighlighter(X.class, queryBuilder.build(), 30).forEach(x -> {
                log.info(x.toString());
            });

   ```

   

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
