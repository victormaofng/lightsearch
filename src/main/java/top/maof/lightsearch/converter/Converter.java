package top.maof.lightsearch.converter;

/**
 * 转换器
 * 对象的值有时候不适合直接创建索引保存,
 * 这个时候就需要转换为其他的值去保存
 * <p>
 * 比如 日期时间 2012-12-12 11:12:12 ,直接建立索引的话是不利于进行时间范围查询的,
 * 可以将其转化为long类型时间戳,用long类型时间戳去建立索引,这个时候就可以进行时间范围查询
 */
public interface Converter<E,T> {

    public Class propertyType();

    public Class filedType();

    /**
     * Object value: 对象属性原本的值
     * 通过这个方法,可以将value转化为其他的值,
     * 再去创建索引
     */
    public T writeCovert(E value);

    /**
     * 搜索时,从索引中获取到了保存的值,但这个值并非
     * 原本对象的属性值,所以需要进行转换,否则可能会出现
     * 类型错误
     */
    public E readCovert(T fieldValue);
}
