package top.maof.lightsearch.test;

import lombok.extern.slf4j.Slf4j;
import org.apache.lucene.document.*;
import org.apache.lucene.search.BooleanClause;
import org.junit.Test;
import top.maof.lightsearch.core.LightSearch;
import top.maof.lightsearch.core.QueryBuilder;
import top.maof.lightsearch.util.LightUtils;
import top.maof.lightsearch.util.PropertyFunction;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class Main {

    @Test
    public void t() throws Exception {
        X x = new X();
        Document document = new Document();
        document.add(new IntPoint("id", x.getId()));
        document.add(new StringField("name", x.getName(), Field.Store.YES));
        document.add(new IntPoint("age", x.getAge()));
        x.getList().forEach(i -> {
            document.add(new IntPoint("list", i));
        });
        document.add(new LongPoint("time", x.getTime()));
        document.add(new StringField("gender", x.isGender() + "", Field.Store.YES));
        document.add(new LongPoint("date", x.getDate().getTime()));

    }

    @Test
    public void search() throws Exception {
        LightSearch lightSearch = LightSearch.getInstance("C:\\Users\\mao\\Desktop\\x");

        QueryBuilder queryBuilder = new QueryBuilder();
        List<PropertyFunction<X>> list = new ArrayList<>();
        list.add(X::getName);
        queryBuilder.tokenizeQuery(X.class, list, "简单", lightSearch.analyzer(), BooleanClause.Occur.SHOULD);
        queryBuilder.numberRange(X.class, X::getAge, 11, 13, BooleanClause.Occur.SHOULD);

        lightSearch.searchHighlighter(X.class, queryBuilder.build(), 1, 30).getList().forEach(x1 -> {
            List l = x1.getList();
            log.info(x1.toString());
        });
    }

    @Test
    public void index() {
        LightSearch lightSearch = LightSearch.getInstance("C:\\Users\\mao\\Desktop\\x");

        String[] names = new String[]{"全文检索引擎lucene", "培训机构", "全球直播",
                "简单工厂模式", "工厂方法模式", "原型模式", "策略模式"};

        List<X> list = new ArrayList<>();
        int i = 0;
        while (i < names.length) {
            X x = new X();
            x.setName(names[i]);
            x.setAge(i * 2);
            List list1 = new ArrayList();
            list1.add(names[i]);
            list1.add(i * 4);
            x.setList(list1);
            list.add(x);
            i++;
        }

        try {
            lightSearch.write(list);
            lightSearch.merge(2);
        } catch (Exception e) {
            e.printStackTrace();
        }

        lightSearch.close();
    }

    @Test
    public void test() throws Exception {
        LightSearch lightSearch = LightSearch.getInstance("C:\\Users\\Administrator\\Desktop\\x");
        X x=new X();
        lightSearch.write(x);
    }
}
