package top.maof.lightsearch.annotation;

import top.maof.lightsearch.converter.Converter;
import top.maof.lightsearch.tag.Tag;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * document文档
 * 主键,可根据主键查询、修改和删除
 * 一个类中只能有一个key属性
 * <p>
 * 不分词,但是索引,是否存储可选
 * 例如: id
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Key {
    // 是否存储
    boolean isStored() default true;

    /**
     * 列名,默认为属性字段值
     */
    String value() default "";

    // 权重,查询所用
    double weight() default 1.0;


    /**
     * 高亮处理时放置关键字的标签
     */
    Class<? extends Tag> tag() default Tag.class;
}
