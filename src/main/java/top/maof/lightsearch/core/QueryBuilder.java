package top.maof.lightsearch.core;

import lombok.Data;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.DoublePoint;
import org.apache.lucene.document.FloatPoint;
import org.apache.lucene.document.IntPoint;
import org.apache.lucene.document.LongPoint;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import top.maof.lightsearch.annotation.Key;
import top.maof.lightsearch.annotation.Property;
import top.maof.lightsearch.exception.LightException;
import top.maof.lightsearch.util.LightUtils;
import top.maof.lightsearch.util.PropertyFunction;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QueryBuilder {

    private final BooleanQuery.Builder builder;

    public QueryBuilder() {
        builder = new BooleanQuery.Builder();
    }

    /**
     * 数字范围
     */
    public <T> QueryBuilder numberRange(Class<T> clazz, PropertyFunction<T> pf, Object lower, Object upper,
                                        BooleanClause.Occur occur) throws LightException {
        Field field = LightUtils.field(clazz, pf);
        String fieldName = LightUtils.fieldKey(clazz, pf, field);
        Query query = null;
        Class aClass = lower.getClass();
        if (aClass == int.class || aClass == Integer.class) {
            query = IntPoint.newRangeQuery(fieldName, (int) lower, (int) upper);
        } else if (aClass == long.class || aClass == Long.class) {
            query = LongPoint.newRangeQuery(fieldName, (long) lower, (long) upper);
        } else if (aClass == float.class || aClass == Float.class) {
            query = FloatPoint.newRangeQuery(fieldName, (float) lower, (float) upper);
        } else if (aClass == double.class || aClass == Double.class) {
            query = DoublePoint.newRangeQuery(fieldName, (double) lower, (double) upper);
        } else if (aClass == Date.class) {
            query = LongPoint.newRangeQuery(fieldName, ((Date) lower).getTime(), ((Date) upper).getTime());
        }
        this.builder.add(new BoostQuery(query, LightUtils.weight(field)), occur);
        return this;

    }

   /**
    * 元词查询
    */
    public <T> QueryBuilder termQuery(Class clazz, PropertyFunction<T> pf, String words, BooleanClause.Occur occur) throws LightException {
        Field field = LightUtils.field(clazz, pf);
        Query query = new TermQuery(new Term(LightUtils.fieldKey(clazz, pf, field), words));
        this.builder.add(new BoostQuery(query, LightUtils.weight(field)), occur);
        return this;
    }

    /**
     * 单词查询
     */
    public <T> QueryBuilder tokenizeQuery(Class clazz, PropertyFunction<T> pf, String words, Analyzer analyzer,
                                          BooleanClause.Occur occur) throws LightException {
        Field field = LightUtils.field(clazz, pf);
        QueryParser parser = new QueryParser(LightUtils.fieldKey(clazz, pf, field), analyzer);
        Query query = null;
        try {
            query = parser.parse(words);
            query = new BoostQuery(query, LightUtils.weight(field));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.builder.add(query, occur);
        return this;
    }


    public <T> QueryBuilder tokenizeQuery(Class clazz, List<PropertyFunction<T>> pfs, String words, Analyzer analyzer,
                                          BooleanClause.Occur occur) throws LightException {
        String[] fieldNames = new String[pfs.size()];
        Map<String, Float> boots = new HashMap<>();
        for (int i = 0; i < pfs.size(); i++) {
            Field filed = LightUtils.field(clazz, pfs.get(i));
            fieldNames[i] = LightUtils.fieldKey(clazz, pfs.get(i), filed);
            boots.put(fieldNames[i], LightUtils.weight(filed));
        }
        QueryParser parser = new MultiFieldQueryParser(fieldNames, analyzer, boots);
        Query query = null;
        try {
            query = parser.parse(words);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.builder.add(query, occur);
        return this;
    }


    public <T> QueryBuilder prefixQuery(Class clazz, PropertyFunction<T> pf, String prefix, BooleanClause.Occur occur)
            throws Exception {
        Field field = LightUtils.field(clazz, pf);
        Query query = new PrefixQuery(new Term(LightUtils.fieldKey(clazz, pf, field), prefix));
        this.builder.add(new BoostQuery(query, LightUtils.weight(field)), occur);
        return this;

    }

    public Query build() {
        return this.builder.build();
    }
}
