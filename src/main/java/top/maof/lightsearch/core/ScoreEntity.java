package top.maof.lightsearch.core;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class ScoreEntity<T> {

    public T entity;

    public float score;
}
