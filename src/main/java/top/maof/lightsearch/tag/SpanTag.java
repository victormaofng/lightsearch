package top.maof.lightsearch.tag;

public class SpanTag implements Tag {

    private final static String preTag = "<span style='color:red'>";

    private final static String postTag = "</span>";

    @Override
    public String preTag() {
        return preTag;
    }

    @Override
    public String postTag() {
        return postTag;
    }
}
