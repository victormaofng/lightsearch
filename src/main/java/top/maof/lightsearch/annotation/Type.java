package top.maof.lightsearch.annotation;

import org.apache.lucene.analysis.Analyzer;
import org.wltea.analyzer.lucene.IKAnalyzer;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 文档类别,用于区分文档
 * <p>
 * 同一个类的对象构建的文档应属于同一个文档类别,
 * 不同的类的对象构建的文档应属于不同的类别
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface Type {
    // 建议用类名或全类名,默认为类名,索引创建后值不能发生变化,否则将查询不到之前创建的索引
    String value() default "";

    /**
     * 建立索引时使用的分词器,默认使用IK
     *
     * 为了提高创建索引速度,建议所有类使用同一个分词器
     */
    @Deprecated
    Class<? extends Analyzer> analyzer() default IKAnalyzer.class;
}
