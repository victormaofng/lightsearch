package top.maof.lightsearch.exception;

public class NoMappingException extends LightException {
    public NoMappingException() {
        super("类型没有映射器");
    }
}
