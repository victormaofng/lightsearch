package top.maof.lightsearch.annotation;

import top.maof.lightsearch.converter.Converter;
import top.maof.lightsearch.tag.Tag;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Property {
    // 是否分词
    boolean isTokenized() default true;

    // 是否索引
    boolean isIndexed() default true;

    // 是否存储
    boolean isStored() default true;

    /**
     * 列名,默认为属性字段值
     */
    String value() default "";

    // 权重
    double weight() default 1.0;

    /**
     * 转换器
     */
    Class<? extends Converter> convert() default Converter.class;

    /**
     * 高亮处理时放置关键字的标签
     */
    Class<? extends Tag> tag() default Tag.class;
}
