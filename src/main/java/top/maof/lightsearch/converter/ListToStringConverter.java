package top.maof.lightsearch.converter;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import lombok.Data;

import java.util.List;

@Data
public class ListToStringConverter implements Converter<List, String> {


    @Override
    public Class propertyType() {
        return List.class;
    }

    @Override
    public Class filedType() {
        return String.class;
    }

    @Override
    public String writeCovert(List value) {
        return JSON.toJSON(value).toString();
    }

    @Override
    public List readCovert(String fieldValue) {
        return JSONArray.parseArray(fieldValue);
    }
}
