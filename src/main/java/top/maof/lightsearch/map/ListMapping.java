package top.maof.lightsearch.map;

import org.apache.lucene.index.IndexableField;

import java.util.ArrayList;
import java.util.List;

public class ListMapping implements Mapping {
    @Override
    public List<IndexableField> javaMappingIndex(Parms parms) throws Exception {
        List<IndexableField> indexableFieldList = new ArrayList<>();
        List list = (List) parms.getJavaValue();
        if (list != null && !list.isEmpty()) {
            for (Object o : list) {
                Mapping mapping = MappingPool.getMapping(o.getClass());
                Parms p = new Parms();
                p.setFieldName(parms.getFieldName());
                p.setJavaValue(o);
                p.setStored(parms.isIndexed());
                p.setIndexed(parms.isIndexed());
                p.setTokenized(parms.isTokenized());
                List<IndexableField> indexableFields = mapping.javaMappingIndex(p);
                indexableFieldList.addAll(indexableFields);
            }
        }
        return indexableFieldList;
    }

    @Override
    public Object indexMappingJava(List<IndexableField> list) throws Exception {
        List objs = new ArrayList();
        if (list != null) {
            list.forEach(i -> {
                Object o = null;
                try {
                    o = i.numericValue();
                } catch (Exception e) {

                } finally {
                    if (o == null)
                        o = i.stringValue();
                    objs.add(o);
                }

            });
        }
        return objs;
    }
}
