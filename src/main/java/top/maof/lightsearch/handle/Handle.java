package top.maof.lightsearch.handle;

/**
 * 处理器
 */
public interface Handle {
    // 索引创建
    void indexHandle(Entity entity, Index index, HandleChain handleChain) throws Exception;

    // 搜索索引
    void searchHandle(Entity entity, Index index, HandleChain handleChain) throws Exception;

    /**
     * 线程安全且可共享的
     * 模仿Nginx的Share注解
     *
     * <p>
     * 建立或搜索索引的过程中，常会涉及到共享对象的情况，
     * 但是部分对象是线程不安全的或是无法共享的,
     * 凡是不符合要求的对象，必须在操作过程中重新实例化
     * 比如：HandleChain、HighlighterHandle，这两个类
     * 的对象就无法共享，必须在使用时实例化新的对象
     */
    @interface Shared {
    }
}
